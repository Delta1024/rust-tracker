use std::process;

/// Recursivly searches up the directory structure to find the specified directory
/// ```
/// fn main() -> std::io::Result<()> {
///  use crawler::search_for_dir;
///  let cur_dir = std::env::current_dir()?;
///
///  let mut cur_dir = cur_dir.into_iter().as_path().to_path_buf();
///  cur_dir.push("tests");
///
///  const SEARCH_FOR: &str = ".tracker";
///  let result = search_for_dir(Box::new(cur_dir.as_path()), SEARCH_FOR, false)?;
///
///  let mut track_dir = cur_dir.clone();
///
///  track_dir.push(SEARCH_FOR);
///  let track_dir = track_dir.as_path();
///  let track_dir = track_dir.display().to_string();
///  assert_eq!(result, track_dir);
///  Ok(())
/// }
/// ```
pub fn search_for_dir(
    cur_dir: std::boxed::Box<&std::path::Path>,
    search_var: &str,
    is_init: bool, // this is faulse to facilitate an exeption when createing a .tracker dir
) -> std::io::Result<String> {
    let cur_dir = *cur_dir.clone();

    let mut temp = cur_dir.to_path_buf();
    temp.push(search_var.clone());
    let temp = temp.as_path();
    let exist_check = temp.exists();
    // returns current dir for initialisation option
    if is_init && !exist_check {
        let mut cur_dir = cur_dir.to_path_buf();
        cur_dir.push(".tracker");

        return Ok(cur_dir.as_path().display().to_string());
    }

    let is_folder = temp.is_dir();
    if exist_check && is_folder {
        return Ok(String::from(temp.to_str().unwrap()));
    } else if !is_folder && !exist_check {
        let up_dir = match cur_dir.parent() {
            Some(a) => a,
            None => {
                eprintln!("no track dir found");

                process::exit(2);
            }
        };
        search_for_dir(Box::new(up_dir), search_var, false)
    } else if !is_folder {
        panic!(".tracker file existes but is not a folder");
    } else {
        panic!("an unknown error has occred");
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn search_for_dir_recursive_check() -> std::io::Result<()> {
        let cur_dir = std::env::current_dir()?;
        let mut cur_dir = cur_dir.into_iter().as_path().to_path_buf();
        cur_dir.push("tests");
        let mut track_dir = cur_dir.clone();
        cur_dir.push("recures_test");
        let cur_dir = Box::new(cur_dir.as_path());
        const SEARCH_FOR: &str = ".tracker";

        track_dir.push(".tracker");
        let track_dir = track_dir.as_path();
        let track_dir = track_dir.display().to_string();
        let result = search_for_dir(cur_dir, SEARCH_FOR, false)?;

        assert_eq!(*result, track_dir);
        Ok(())
    }

    #[test]
    fn search_for_dir_deep_recurse_test() -> std::io::Result<()> {
        let cur_dir = std::env::current_dir()?;
        let mut cur_dir = cur_dir.into_iter().as_path().to_path_buf();
        cur_dir.push("tests");
        let mut track_dir = cur_dir.clone();
        cur_dir.push("recures_test");
        cur_dir.push("deep_test");
        let cur_dir = Box::new(cur_dir.as_path());
        const SEARCH_FOR: &str = ".tracker";

        track_dir.push(".tracker");
        let track_dir = track_dir.as_path();
        let track_dir = track_dir.display().to_string();
        let result = search_for_dir(cur_dir, SEARCH_FOR, false)?;

        assert_eq!(result, track_dir);
        Ok(())
    }

}
