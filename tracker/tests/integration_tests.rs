use app_options::init_track_dir;
use std::fs::{self}; // File};
use std::io;
// use std::path::Path;
use tracker::app_options::{self, RequestInfo ,  SaveLoad};

mod common;
use self::common::{init_setup, Branch};

#[test]
#[should_panic(expected = "already a tracker repo")]
#[allow(unused_must_use)]
fn test_init_track_dir_fail() -> () {
    let (cur_dir, _optoins, _track_dir, _check) = init_setup(Branch::Character, true);
    let mut track_dir = cur_dir;
    track_dir.push_str("_FAIL");
    init_track_dir(&track_dir);
}

#[test]
fn test_init_track_dir() -> std::io::Result<()> {
    let (_cur_dir, _optoins, track_dir, _check) = init_setup(Branch::Character, true);
    init_track_dir(&track_dir)?;
    let mut file_list = fs::read_dir(&track_dir)?
        .map(|res| res.map(|e| e.path()))
        .collect::<Result<Vec<_>, io::Error>>()?;
    file_list.sort();
    let mut files: Vec<String> = vec![];
    for i in file_list {
        let a = i.as_path().file_stem().unwrap().to_str().unwrap();
        files.push(String::from(a));
    }
    let check: Vec<String> = vec!["CHAR".to_string(), "DOC".to_string(), "SCENE".to_string()];
    std::fs::remove_dir_all(&track_dir)?;

    assert_eq!(files, check);

    Ok(())
}

#[test]
fn test_create_char() -> std::io::Result<()> {
    let (_cur_dir, options, tracker_dir, input) = init_setup(Branch::Character, false);
    app_options::create_file(tracker_dir.clone(), options.clone(), input, false)?;
    let check = vec!["hello".to_string(), "my friend".to_string()];
    let mut configure = RequestInfo::new(false);
    let configure = configure.build(&tracker_dir, &options)?;
    let configure = vec![configure.primary.clone(), configure.secondary.clone()];
    let mut cleanup = tracker_dir.clone();
    cleanup.push_str("/CHAR/hello");
    std::fs::remove_file(cleanup)?;
    assert_eq!(configure, check);
    Ok(())
}

#[test]
fn test_create_scene() -> std::io::Result<()> {
    let (_cur_dir, options, tracker_dir, input) = init_setup(Branch::Scene, false);
    app_options::create_file(tracker_dir.clone(), options.clone(), input, true)?;
    let check = vec!["my friend".to_string(), "hello".to_string()];
    let mut configure = RequestInfo::new(true);
    let configure = configure.build(&tracker_dir, &options)?;
    let configure = vec![configure.primary.clone(), configure.secondary.clone()];
    let mut tracker_dir = tracker_dir;
    tracker_dir.push_str("/SCENE/my friend");
    std::fs::remove_file(tracker_dir)?;
    assert_eq!(check, configure);
    Ok(())
}
