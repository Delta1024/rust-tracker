use std::env;
pub enum Branch {
    Character,
    Scene,
}

/// returns (cur_dir, options, tracker_dir, check)
#[allow(dead_code)]
pub fn setup(check_opts: Branch) -> (String, String, String, String) {
    let mut cur_dir = env::current_dir().unwrap().display().to_string();
    let mut tracker_dir = cur_dir.clone();
    tracker_dir.push_str("/tests/.tracker");
    let options = match &check_opts {
        Branch::Character => String::from("hello"),
        Branch::Scene => String::from("my friend"),
    };
    let check = match &check_opts {
        Branch::Character => String::from("my friend"),
        Branch::Scene => String::from("hello"),
    };
    cur_dir.push_str("/tests");
    (cur_dir, options, tracker_dir, check)
}

/// returns (cur_dir, options, tracker_dir, check) for init functions
pub fn init_setup(check_opts: Branch, is_init: bool) -> (String, String, String, String) {
    let mut cur_dir = env::current_dir().unwrap().display().to_string();
    let mut tracker_dir = cur_dir.clone();
    let options = match check_opts {
        Branch::Character => String::from("hello"),
        Branch::Scene => String::from("my friend"),
    };
    let check = match &check_opts {
        Branch::Character => String::from("my friend"),
        Branch::Scene => String::from("hello"),
    };
    if !is_init {
        tracker_dir.push_str("/tests/test_objects/.tracker");
        cur_dir.push_str("/tests/test_objects/");
    } else {
        tracker_dir.push_str("/tests/test_objects/init_test/.tracker");
        cur_dir.push_str("/tests/test_objects/init_test");
    }
    (cur_dir, options, tracker_dir, check)
}
