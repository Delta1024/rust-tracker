//! Tracker is an application for handling the tracking of what character is in each
//! scene and also what other characters are relevent.
use std::{path::Path, process};
pub mod app_options;
pub mod option_logic;
use crawler::search_for_dir;
use option_logic::{get_char, get_scene, new_char, new_scene};

pub mod option_conig {
    use getopts::{Options, ParsingStyle};
    pub fn print_usage(program: &str, opts: Options) {
        let brief = format!("Usage:{} [options] CHAR/SCENE", program);
        println!("{}", opts.usage(&brief));
    }

    pub fn opt_flags() -> Options {
        let mut opts = Options::new();
        opts.parsing_style(ParsingStyle::FloatingFrees);
        opts.optflag(
            "I",
            "init",
            "initialise a new tracker project in current directory",
        );

        opts.optflagopt(
            "c",
            "character",
            "get character information",
            "Character Scene",
        );
        opts.optflagopt("s", "scene", "gets scene information", "Scene Character");

        opts.optflagopt("l", "list", "list all tracked info", "Scene Character");
        opts.optflagopt(
            "n",
            "new",
            "creates a new character or scene file file",
            "Character Scene",
        );

        opts.optflag("h", "print this help menu", "help");

        opts.optflagopt(
            "g",
            "get",
            "get tracked characters or scenes for specified character/scene",
            "Scene/Character",
        );

        opts
    }
}
use self::option_conig::{opt_flags, print_usage};

pub fn run(
    search_for_const: &str,
    cur_dir: Box<&Path>,
    args: Vec<String>,
    program: String,
) -> Result<(), std::io::Error> {
    // opts, matches
    //      user option flag declerations

    let opts = opt_flags();

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => std::panic::panic_any(f.to_string()),
    };

    let work_dir = search_for_dir(cur_dir, search_for_const, false)?;

    if matches.opt_present("h") {
        print_usage(&program, opts);
        return Ok(());
    }

    if matches.opt_present("l") {
        let options = matches.opt_str("l").unwrap();
        let options = options.as_str();

        match options {
            "c" => {
                app_options::list_spec_dir(&work_dir, false)?;
                return Ok(());
            }
            "s" => {
                app_options::list_spec_dir(&work_dir, true)?;
                return Ok(());
            }
            _ => {
                print_usage(&program, opts);
                return Ok(());
            }
        }
    }

    let input = if !matches.free.is_empty() {
        matches.free[0].clone()
    } else if matches.opt_present("I") {
        // here we check for the initalization option
        match app_options::init_track_dir(&work_dir) {
            Ok(()) => return Ok(()),
            Err(e) => {
                eprintln!("Applicatoin Error: {}", e);

                process::exit(3);
            }
        }
    } else {
        print_usage(&program, opts);
        return Ok(());
    };

    if matches.opt_present("g") {
        let options = matches.opt_str("g").unwrap();
        let options = options.as_str();

        match options {
            "c" => {
                get_char(work_dir, input)?;

                return Ok(());
            }
            "s" => {
                get_scene(work_dir, input)?;

                return Ok(());
            }
            _ => {
                print_usage(&program, opts);
                return Ok(());
            }
        }
    }

    if matches.opt_present("n") {
        let options = matches.opt_str("n").unwrap();
        let options = options.as_str();

        match options {
            "c" => {
                new_char(work_dir, input, matches.clone(), program, opts)?;
                return Ok(());
            }
            "s" => {
                new_scene(work_dir, input, matches.clone(), program, opts)?;
                return Ok(());
            }
            _ => {
                print_usage(&program, opts);
            }
        }
    }
    Ok(())
}
