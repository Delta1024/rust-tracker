use std::{env, path::Path, process};
use tracker::run;

fn main() {
    const SEARCH_FOR_DIR: &str = ".tracker"; // set the name of the folder to check for

    let cur_dir = env::current_dir().unwrap().display().to_string();

    let args: Vec<String> = env::args().collect();

    let program = args[0].clone();

    let cur_dir = Box::new(Path::new(&cur_dir).as_ref());
    //checks for and returns the location of the specified directory
    match run(SEARCH_FOR_DIR, cur_dir, args, program) {
        Ok(()) => return,
        Err(e) => {
            eprintln!("Applicatoin Error, {}", e);
            process::exit(1);
        }
    }
}
