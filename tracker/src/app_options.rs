//! This module houses general application options and the SaveLoad trait
//! This module holds all of the character related functions and instantiations
use std::collections::HashMap;
use std::fs::{self, File, OpenOptions};
use std::io::{
    self,
    prelude::{BufRead, Read},
};
use std::path::Path;

fn make_dir_structure(track_dir: String) -> std::io::Result<()> {
    let mut char_dir = track_dir.clone();
    char_dir.push_str("/CHAR");

    let mut scene_dir = track_dir.clone();
    scene_dir.push_str("/SCENE");

    let mut master_doc_dir = track_dir.clone();
    master_doc_dir.push_str("/DOC");

    let dirs_to_make = vec![char_dir, scene_dir, master_doc_dir];

    for i in dirs_to_make {
        // println!("{}", i);
        fs::create_dir_all(i)?;
    }
    Ok(())
}

pub fn init_track_dir(track_dir: &str) -> std::io::Result<()> {
    let check = Path::new(&track_dir).exists();
    if !check {
        // code to make dir structure goes here
        println!("creating tracker directory");
        make_dir_structure(track_dir.to_string())?;
        return Ok(());
    } else {
        panic!("already a tracker repo");
    }
}

pub trait SaveLoad {
    fn to_vec(&mut self) -> Vec<String> {
        unimplemented!();
    }
    fn aux_to_vec(&mut self) -> Vec<String> {
        unimplemented!();
    }

    fn build(&mut self, td: &str, q: &str) -> std::io::Result<&mut Self>;

    fn bridge(self) -> std::io::Result<Box<HashMap<String, Vec<String>>>>;

    fn list(&mut self, td: &str, q: &str) -> std::io::Result<(String, String)> {
        unimplemented!("{} {}", td, q);
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct RequestInfo {
    pub track_dir: String,
    pub primary: String,
    pub secondary: String,
    pub is_scene: bool,
}

impl RequestInfo {
    pub fn new(t: bool) -> Self {
        RequestInfo {
            track_dir: String::new(),
            primary: String::new(),
            secondary: String::new(),
            is_scene: t,
        }
    }
}
impl SaveLoad for RequestInfo{
    fn build(&mut self, track_dir: &str, primary: &str) -> std::io::Result<&mut Self> {
	let spec_dir = match self.is_scene {
	    false => "/CHAR/",
	    true => "/SCENE/",
	};
        self.primary.push_str(&primary);
        self.track_dir.push_str(track_dir);
        self.track_dir.push_str(spec_dir);

        let mut n = self.track_dir.clone();
        n.push_str(primary);

        let mut char_scene = File::open(n)?;
        char_scene.read_to_string(&mut self.secondary)?;
        Ok(self)
    }

    fn bridge(self) -> std::io::Result<Box<HashMap<String, Vec<String>>>> {
	let spec_dir = match self.is_scene {
	    true => "CHAR",
	    false => "SCENE",
	};
        let track_dir = Path::new(&self.track_dir);
        let track_dir = track_dir.parent().unwrap();
        let mut track_dir = track_dir.to_path_buf();
        track_dir.push(spec_dir);

        let mut scene_names: Vec<String> = Vec::new();
        let mut map = HashMap::new();
        for i in self.secondary.lines() {
            let mut temp_track = track_dir.clone();
            temp_track.push(i.to_string());
            scene_names.push(temp_track.as_path().display().to_string());
            map.insert(i.to_string(), vec![]);
        }

        for i in scene_names {
            let file_name = i.clone();
            let file_name = Path::new(&file_name);
            let file_name = file_name.file_stem().unwrap().to_str().unwrap();
            let mut file = String::new();
            let _n = OpenOptions::new()
                .read(true)
                .open(&i)
                .unwrap()
                .read_to_string(&mut file);
            for i in file.lines() {
                let file_lines = map.entry(file_name.to_string()).or_insert(vec![]);
                file_lines.push(i.to_string());
                // map.insert(file_name.to_string(), vec![i.to_string()]);
            }
        }
        // see below for link to solution for three line problem
        /* https://static.rust-lang.org/doc/master/std/collections/hash_map/struct.HashMap.html#method.entry */
        Ok(Box::new(map))
    }

    fn list(&mut self, track_dir: &str, character: &str) -> io::Result<(String, String)> {
        self.build(&track_dir, &character)?;
        let primary = self.primary.clone().to_string();
        let secondary = self.secondary.clone().to_string();
        Ok((primary, secondary))
    }

    fn aux_to_vec(&mut self) -> Vec<String> {
        let cursor = io::Cursor::new(self.secondary.as_bytes());

        let result: Vec<String> = cursor.lines().map(|l| l.unwrap()).collect();
        result
    }
}

/// crates a character file in the CHAR directory
pub fn create_file(track_dir: String, primary: String, secondary: String, is_scene: bool) -> std::io::Result<()> {
    let mut active_file = RequestInfo{
        track_dir,
        primary,
        secondary,
	is_scene,
    };
    let spec_dir = match is_scene {
	true => "/SCENE/",
	false => "/CHAR/",
    };

    let opt_reflect = match is_scene {
	true => "scene",
	false =>"character",
    };
    active_file.track_dir.push_str(spec_dir);
    active_file.track_dir.push_str(&active_file.primary);
    println!("Creating file for {}: {}", opt_reflect, active_file.primary);
    let check = Path::new(&active_file.track_dir).exists();
    if !check {
        fs::write(active_file.track_dir, active_file.secondary)?;
    } else {
        eprintln!(
            "{} file already exists, please use the -e option to edit the file instead", opt_reflect
        );
    }

    Ok(())
}

pub fn list_spec_dir(track_dir: &str, is_scene: bool) -> std::io::Result<()> {
    let spec_dir = match is_scene {
	true => "SCENE",
	false => "CHAR",
    };
    let mut work_dir = Path::new(track_dir).to_path_buf();
    work_dir.push(spec_dir);
    let work_dir = work_dir.as_path().display().to_string();

    let mut entries = fs::read_dir(work_dir)?
        .map(|res| res.map(|e| e.path()))
        .collect::<Result<Vec<_>, io::Error>>()?;

    entries.sort();

    let opt_reflect = match is_scene {
	true => "Scene",
	false => "Character",
    };
    println!("Tracked {}:", opt_reflect);
    for i in entries {
        println!("{}", i.as_path().file_stem().unwrap().to_str().unwrap());
    }
    Ok(())
}
#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn char_list_check() -> std::io::Result<()> {
        let track_dir = "tests/.tracker";
        let character = "hello";
        let check = (character.to_string(), "world".to_string());
        let mut fake_char = RequestInfo::new(false);
        assert_eq!(fake_char.list(track_dir, character)?, check);
        Ok(())
    }

    #[test]
    fn char_build_check() -> std::io::Result<()> {
        let track_dir = "tests/.tracker";
        let char_name = "hello";
        let mut character = RequestInfo::new(false);
        let character = character.build(track_dir, char_name)?;
        let mut track_dir = track_dir.to_string();
        track_dir.push_str("/CHAR/");
        let ref char_check = RequestInfo {
            track_dir: track_dir.to_string(),
            primary: char_name.to_string(),
            secondary: "world".to_string(),
	    is_scene: false,
        };
        assert_eq!(char_check, character);

        Ok(())
    }

    #[test]
    fn char_bridge_scene_test() {
        let character = RequestInfo {
            track_dir: "tests/.tracker/CHAR".to_string(),
            primary: "hello".to_string(),
            secondary: "world".to_string(),
	    is_scene: false,
        };

        let n = character.bridge().unwrap();
        let mut hmap = HashMap::new();
        hmap.insert(String::from("world"), vec![String::from("hello")]);
        assert_eq!(hmap, *n);
    }

    #[test]
    fn char_bridge_scene_two_line_from_source_test() {
        let character = RequestInfo {
            track_dir: "tests/.tracker/CHAR".to_string(),
            primary: "goodby".to_string(),
            secondary: "planet\nsystem".to_string(),
	    is_scene: false,
        };

        let n = character.bridge().unwrap();
        let mut hmap = HashMap::new();
        hmap.insert(String::from("planet"), vec![String::from("goodby")]);
        hmap.insert(String::from("system"), vec![String::from("goodby")]);

        assert_eq!(hmap, *n);
    }

    #[test]
    fn char_bridge_scene_two_line_in_dest_test() {
        let character = RequestInfo {
            track_dir: "tests/.tracker/CHAR".to_string(),
            primary: "salutations".to_string(),
            secondary: "mars".to_string(),
	    is_scene: false,
        };

        let n = character.bridge().unwrap();
        let mut hmap = HashMap::new();
        hmap.insert(
            String::from("mars"),
            vec![String::from("greetings"), String::from("salutations")],
        );

        assert_eq!(hmap, *n);
    }

    #[test]
    fn scene_list_check() -> std::io::Result<()> {
        let track_dir = "tests/.tracker";
        let scene = "world";
        let check = (scene.to_string(), "hello".to_string());
        let mut fake_char = RequestInfo::new(true);
        assert_eq!(fake_char.list(track_dir, scene)?, check);
        Ok(())
    }

    #[test]
    fn scene_build_check() -> std::io::Result<()> {
        let track_dir = "tests/.tracker";
        let scene_name = "world";
        let mut scene = RequestInfo::new(true);
        let scene = scene.build(track_dir, scene_name)?;
        let mut track_dir = track_dir.to_string();
        track_dir.push_str("/SCENE/");
        let ref scene_check = RequestInfo {
            track_dir: track_dir.to_string(),
            primary: scene_name.to_string(),
            secondary: "hello".to_string(),
	    is_scene: true,
        };
        assert_eq!(scene_check, scene);

        Ok(())
    }

    #[test]
    fn scene_bridg_char_test() {
        let scene = RequestInfo {
            track_dir: "tests/.tracker/SCENE".to_string(),
            primary: "world".to_string(),
            secondary: "hello".to_string(),
	    is_scene: true,
        };

        let n = scene.bridge().unwrap();
        let mut hmap = HashMap::new();
        hmap.insert(String::from("hello"), vec![String::from("world")]);
        assert_eq!(hmap, *n);
    }

    #[test]
    fn scene_bridge_char_two_line_from_source_test() {
        let scene = RequestInfo {
            track_dir: "tests/.tracker/SCENE".to_string(),
            primary: "mars".to_string(),
            secondary: "greetings\nsalutations".to_string(),
	    is_scene: true,
        };

        let n = scene.bridge().unwrap();
        let mut hmap = HashMap::new();
        hmap.insert(String::from("greetings"), vec![String::from("mars")]);
        hmap.insert(String::from("salutations"), vec![String::from("mars")]);
        assert_eq!(hmap, *n);
    }

    #[test]
    fn scene_bridge_char_two_line_in_dest_test() {
        let scene = RequestInfo {
            track_dir: "tests/.tracker/SCENE".to_string(),
            primary: "venus".to_string(),
            secondary: "wave".to_string(),
	    is_scene: true,
        };

        let n = scene.bridge().unwrap();
        let mut hmap = HashMap::new();
        hmap.insert(
            String::from("wave"),
            vec![String::from("jupitor"), String::from("venus")],
        );
        assert_eq!(hmap, *n);
    }
}
