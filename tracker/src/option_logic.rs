use super::app_options::{self, RequestInfo, SaveLoad};
use super::option_conig::print_usage;
pub fn get_char(work_dir: String, input: String) -> std::io::Result<()> {
    let mut result = RequestInfo::new(false);
    let (character, scene) = result.list(&work_dir, &input)?;
    println!("Scenes for {}:\n{}", character, scene);

    Ok(())
}

pub fn get_scene(work_dir: String, input: String) -> std::io::Result<()> {
    let mut result = RequestInfo::new(true);
    let (scene, character) = result.list(&work_dir, &input)?;

    println!("Characters for {}:\n{}", scene, character);
    Ok(())
}

pub fn new_char(
    work_dir: String,
    input: String,
    matches: getopts::Matches,
    program: String,
    opts: getopts::Options,
) -> std::io::Result<()> {
    let arg = matches.free.clone().into_iter().count();

    let scene_name = if arg <= 2 {
        &matches.free[1]
    } else {
        print_usage(&program, opts);

        return Ok(());
    };
    let local_input = input.clone();
    app_options::create_file(work_dir, scene_name.to_string(), local_input, false)?;
    return Ok(());
}

pub fn new_scene(
    work_dir: String,
    input: String,
    matches: getopts::Matches,
    program: String,
    opts: getopts::Options,
) -> std::io::Result<()> {
    let args = matches.free.clone().into_iter().count();
    let char_name = if args <= 2 {
        &matches.free[1]
    } else {
        print_usage(&program, opts);

        return Ok(());
    };

    app_options::create_file(work_dir, input.clone(), char_name.to_string(), true)?;
    return Ok(());
}
